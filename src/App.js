import React, { useState, useEffect } from "react";

const IntervalExample = () => {
  const [stop, setStop] = useState();
  const [starts, setStarts] = useState();
  const [ends, setEnds] = useState();
  const [seconds, setSeconds] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setSeconds((seconds) => seconds + 1);
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  const clickTimestop = (_s) => {
    const toArray = (num) => num && [...`${num}`].map((elem) => parseInt(elem));
    let _a = toArray(_s)[toArray(_s).length - 1] === 0 ? 0 : 1;
    let _b = _s - 1;
    const start = Math.floor((_a === 0 ? _b : _s) / 10) * 10  ;
    const end = Math.ceil(_s / 10) * 10;
    setStop(start);
    setStarts(start === 0 ? 1 : start + 1);
    setEnds(end === 0 ? 10 : end);
  };

  return (
    <div className="App">
      <header className="App-header">
        {seconds}
        <br />
        {stop}
        <br />
        {starts} - {ends} <br />
        <button onClick={() => clickTimestop(101)}>stop</button>
      </header>
    </div>
  );
};

export default IntervalExample;
